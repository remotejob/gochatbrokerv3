package ads

import (
	"math/rand"

	"gitlab.com/remotejob/gochatbrokerv3/internal/configbroadcast"
)

func Getphrase(conf *configbroadcast.Config) string {

	rnam := rand.Intn(len(conf.Adsslice))

	return conf.Adsslice[rnam]
	
}