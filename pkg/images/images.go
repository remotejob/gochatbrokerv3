package images

import (
	"encoding/json"
	"net/http"
	"strconv"

	"gitlab.com/remotejob/gochatbrokerv3/internal/domains"
)

var (
	images []domains.Imgobj
	urlpost string
)

func Get(urlstr string, quant int, randp bool) ([]domains.Imgobj, error) {

	if randp {

		urlpost = urlstr + "/getimagesrand"

	} else {
		urlpost = urlstr + "/getimages"

	}


	req, err := http.NewRequest("POST", urlpost, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("X-QUANT", strconv.Itoa(quant))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	client := &http.Client{}
	r, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer r.Body.Close()

	json.NewDecoder(r.Body).Decode(&images)

	return images, nil

}
