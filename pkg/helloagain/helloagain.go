package helloagain

import (
	"math/rand"

	"gitlab.com/remotejob/gochatbrokerv3/internal/configbroadcast"
)

func Getphrase(conf *configbroadcast.Config) string {

	rnam := rand.Intn(len(conf.Helloagainslice))

	return conf.Helloagainslice[rnam]
	
}