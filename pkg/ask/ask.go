package ask

import (
	"log"
	"strings"

	"gitlab.com/remotejob/gochatbrokerv3/internal/cleanstring"
	"gitlab.com/remotejob/gochatbrokerv3/internal/configbroadcast"
	"gitlab.com/remotejob/gochatbrokerv3/internal/domains"
	"gitlab.com/remotejob/gochatbrokerv3/internal/utils"
)

var (
	ads    bool
	ansres string
	err    error
)

func Elab(conf *configbroadcast.Config, nguid string, wsclient domains.Wsclient) (bool, string, string, int, error) {

	var ansres string
	var joinask int

	wordsok, askclean := cleanstring.Elab(wsclient.Ask)

	if askclean == wsclient.Lastask {

		return true, askclean, ansres, joinask, nil

	}

	if wordsok && len(askclean) > 2 {

		preproceed := strings.Fields(askclean)

		if len(preproceed) < 2 && (wsclient.Lastask != "") {

			wordsokprev, askcleanprev := cleanstring.Elab(wsclient.Lastask)

			if wordsokprev && len(askcleanprev) > 2 {

				joinask = 1

				askclean = askcleanprev + " " + askclean

			}

		}

		ansres, err = utils.MlService(conf.Mlservice.Url, askclean)
		if err != nil {

			return ads, askclean, ansres, joinask, err
		}

		if ansres == wsclient.Lastanswer {

			log.Println("!!!bot the same answer so ADS!")

			return true, askclean, ansres, joinask, nil
		}

	} else {
		return true, askclean, ansres, joinask, nil

	}

	return ads, askclean, ansres, joinask, nil

}
