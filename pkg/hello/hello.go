package hello

import (
	"math/rand"

	"gitlab.com/remotejob/gochatbrokerv3/internal/configbroadcast"
)

func Getphrase(conf *configbroadcast.Config) string {

	rnam := rand.Intn(len(conf.Greetingslice))

	return conf.Greetingslice[rnam]
	
}