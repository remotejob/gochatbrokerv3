package dbhandler

import (
	"database/sql"
	"encoding/json"
	"log"
	"time"

	"gitlab.com/remotejob/gochatbrokerv3/internal/domains"
	"gitlab.com/remotejob/gochatbrokerv3/internal/utils"
)

var (
	client    domains.Client
	newclient bool
)

func InsertChatRec(db *sql.DB, rec domains.ChatDialog) error {

	stmtstr := "insert INTO mlchatdb(nguid,chatuuid,textinorg,textin,textout,source,class,score,joinask) values(?,?,?,?,?,?,?,?,?)"

	stmt, err := db.Prepare(stmtstr)
	if err != nil {
		return err
	}

	_, err = stmt.Exec(rec.Nguid, rec.Chatuuid, rec.Textinorg, rec.Textin, rec.Textout, rec.Source, rec.Class, rec.Score,rec.Joinask)
	if err != nil {
		return err
	}

	return nil
}

// func Checkhistory(db *sql.DB, id string) (bool, error) {

// 	return true,nil

// }

func Gethistory(db *sql.DB, id string) (bool, []domains.Imgobj, error) {

	stmtstr := "select json(history) from clients where id=?"

	var result bool
	var history string
	var objhistory []domains.Imgobj

	row := db.QueryRow(stmtstr, id)

	switch err := row.Scan(&history); err {

	case sql.ErrNoRows:
		log.Println("NO lastimg??")

	case nil:

		err = json.Unmarshal([]byte(history), &objhistory)
		if err != nil {
			return result, objhistory, err
		}

		log.Println("history", len(objhistory))

		if len(objhistory) > 0 {

			result = true

		}

	case err:
		return result, objhistory, err

	}

	return result, objhistory, nil
}

func Addhistory(db *sql.DB, id string, img domains.Imgobj) error {

	log.Println("DB Addhistory")

	img.CreatedAt = time.Now()

	stmtstr := "select json(lastimg),json(history) from clients where id=?"

	var lastimg string
	var history string
	var modlastimg domains.Imgobj
	var modhistory []domains.Imgobj

	row := db.QueryRow(stmtstr, id)
	switch err := row.Scan(&lastimg, &history); err {

	case sql.ErrNoRows:

		log.Println("NO lastimg?? no client???? ", sql.ErrNoRows)

		return sql.ErrNoRows

	case nil:

		err := json.Unmarshal([]byte(lastimg), &modlastimg)
		if err != nil {
			panic(err)
		}
		err = json.Unmarshal([]byte(history), &modhistory)
		if err != nil {
			panic(err)
		}

		modhistory = append(modhistory, modlastimg)
		modhistory = utils.MakeUniqHistory(modhistory)

	case err:

		log.Println(err)

	}

	stmtstr = "update clients set lastimg=json(?),history=json(?) where id=?"

	imgjsonbyte, _ := json.Marshal(img)
	historybyte, _ := json.Marshal(modhistory)
	stmt, err := db.Prepare(stmtstr)
	if err != nil {
		return err
	}

	_, err = stmt.Exec(imgjsonbyte, historybyte, id)
	if err != nil {
		return err
	}

	stmtstr = "insert INTO bestimg(id) values(?) ON CONFLICT(id) DO UPDATE SET selected=selected+1"

	stmt, err = db.Prepare(stmtstr)
	if err != nil {
		return err
	}

	_, err = stmt.Exec(img.ID)
	if err != nil {
		return err
	}

	return nil

}

func InsertClient(db *sql.DB, id string, wsclient domains.Wsclient, urlimg string) (domains.Client, bool, error) {

	var created time.Time
	var updated time.Time
	var lastimg string
	var history sql.NullString
	var lastimgobj domains.Imgobj

	stmtstr := "select id,created,updated,cookie,registration,lastimg,history from clients where id=?"

	row := db.QueryRow(stmtstr, id)
	switch err := row.Scan(&client.ID, &created, &updated, &client.Cookie, &client.Registration, &lastimg, &history); err {
	case sql.ErrNoRows:

		client.ID = id
		client.Cookie = id

		imgobj, err := utils.GetImg(urlimg, wsclient.Img)
		if err != nil {

			return client, false, err
		}
		// log.Println("addhistory",imgobj)
		imgobj.CreatedAt = time.Now()
		var history []domains.Imgobj

		stmtstr := "insert or ignore INTO clients(id,cookie,lastimg,history) values(?,?,json(?),json(?))"

		imgjsonbyte, _ := json.Marshal(imgobj)
		historybyte, _ := json.Marshal(history)

		stmt, err := db.Prepare(stmtstr)
		if err != nil {
			return client, false, err
		}

		_, err = stmt.Exec(id, id, imgjsonbyte, historybyte)
		if err != nil {
			return client, false, err
		}

		return client, true, err

	case nil:
		client.Created = created
		client.Updated = updated
		err := json.Unmarshal([]byte(lastimg), &lastimgobj)
		if err != nil {

			return client, false, err

		}
		client.Lastimg = lastimgobj

	default:

		return client, false, err
	}

	return client, newclient, nil

}
