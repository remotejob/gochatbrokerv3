package elabstatus

import (
	"database/sql"
	"encoding/json"
	"log"
	"time"

	uuid "github.com/satori/go.uuid"
	"gitlab.com/remotejob/gochatbrokerv3/internal/configbroadcast"
	"gitlab.com/remotejob/gochatbrokerv3/internal/domains"
	"gitlab.com/remotejob/gochatbrokerv3/internal/utils"
	"gitlab.com/remotejob/gochatbrokerv3/pkg/ads"
	"gitlab.com/remotejob/gochatbrokerv3/pkg/ask"
	"gitlab.com/remotejob/gochatbrokerv3/pkg/dbhandler"
	"gitlab.com/remotejob/gochatbrokerv3/pkg/hello"
	"gitlab.com/remotejob/gochatbrokerv3/pkg/helloagain"
)

var (
	err    error
	client domains.Client
	newcl  bool
)

func Elab(conf *configbroadcast.Config, k string, v domains.Wsclient, sleepdur time.Duration) {


	msg := domains.Broadcastmsg{ID: uuid.NewV4().String(), Nguid: k, Payload: `{}`}

	switch v.Status {
	case "open":

		if time.Now().Sub(v.Created) < sleepdur {

			emp := make(map[string]interface{})
			emp["answer"] = hello.Getphrase(conf)
			emp["historyok"] = false
			empData, err := json.Marshal(emp)
			if err != nil {
				log.Panicln(err)
			}

			msg.Type = "start"
			msg.Payload = string(empData)
			utils.Sendmsg(conf.Urlmsg, msg)

		}

	case "start":
	
		client, newcl, err = dbhandler.InsertClient(conf.Db, k, v, conf.Urlimg)
		if err != nil {

			log.Panicln(err)
		}

		if newcl {

			msg.Type = "newclient"

			utils.Sendmsg(conf.Urlmsg, msg)

		} else {

			historyOK, _, err := dbhandler.Gethistory(conf.Db, k)
			if err != nil {
				log.Panicln(err)

			}

			emp := make(map[string]interface{})
			emp["answer"] = helloagain.Getphrase(conf)
			emp["historyok"] = historyOK
			emp["img"] = client.Lastimg
			empData, err := json.Marshal(emp)
			if err != nil {
				log.Panicln(err)
			}

			msg.Type = "oldclient"
			msg.Payload = string(empData)

			utils.Sendmsg(conf.Urlmsg, msg)

		}
	case "ask":


		adsok, askclean, answer,joinask, err := ask.Elab(conf, k, v)
		if err != nil {
			log.Panicln(err)
		}

		if adsok {

			msg.Type = "ads"
			answer = ads.Getphrase(conf)

		} else {

			msg.Type = "answer"

			chatdialog := domains.ChatDialog{Nguid: k, Chatuuid: v.Img, Textinorg: v.Ask, Textin: askclean, Textout: answer, Source: "", Class: "", Score: 0.0,Joinask: joinask}
			err := dbhandler.InsertChatRec(conf.Db, chatdialog)
			if err != nil {
				log.Panicln(err)
			}
		}
		emp := make(map[string]interface{})
		emp["answer"] = answer
		empData, err := json.Marshal(emp)
		if err != nil {
			log.Panicln(err)
		}

		msg.Payload = string(empData)
		utils.Sendmsg(conf.Urlmsg, msg)

	case "answer":
		now := time.Now()
		diffmin := now.Sub(v.Updated).Minutes()

		log.Println("ans", diffmin, v.Wakeup)
		if diffmin > 1 {

			log.Println("wakup History? ", v.History)

		}

	case "addhistory":

		imgobj, err := utils.GetImg(conf.Urlimg, v.Img)
		if err != nil {

			log.Panicln(err)
		}
		err = dbhandler.Addhistory(conf.Db, k, imgobj)
		if err != nil {

			if err == sql.ErrNoRows {

				log.Println("Client not created CREATE!!!")

				client, newcl, err = dbhandler.InsertClient(conf.Db, k, v, conf.Urlimg)
				if err != nil {

					log.Panicln(err)
				}

			} else {
				log.Panicln(err)

			}
		}

		msg.Type = "newimg"

		msg.Payload = string(`{}`)

		utils.Sendmsg(conf.Urlmsg, msg)

	case "newimg":

		log.Println("newimg")

		msg.Type = "answer"
		emp := make(map[string]interface{})
		emp["answer"] = hello.Getphrase(conf)
		empData, err := json.Marshal(emp)
		if err != nil {
			log.Panicln(err)
		}

		msg.Payload = string(empData)
		utils.Sendmsg(conf.Urlmsg, msg)

	case "sendimgs":
		log.Println("sendimgs", v)

	case "gethistory":
		log.Println("gethistory", v)

		historyOK, hisoryobjs, err := dbhandler.Gethistory(conf.Db, k)
		if err != nil {

			log.Panicln(err)
		}

		if historyOK {
			log.Println("historyOK", hisoryobjs)

			msg.Type = "gethistory"

			emp := make(map[string]interface{})
			emp["history"] = hisoryobjs
			empData, err := json.Marshal(emp)
			if err != nil {
				log.Panicln(err)
			}

			msg.Payload = string(empData)
			utils.Sendmsg(conf.Urlmsg, msg)

		}

	}

}
