package cleanstring

import (
	"regexp"
	"strings"
)

func Elab(text string) (bool, string) {

	reg := regexp.MustCompile("[^\\p{L}\\d_?!.]+")
	renum := regexp.MustCompile("[0-9]+")

	preproceed := strings.Fields(text)

	var onlywords []string

	if len(preproceed) > 0 && len(preproceed) < 32 {

		for _, prstr := range preproceed {

			prstr = strings.TrimSpace(prstr)

			nnum := renum.FindAllString(prstr, -1)

			if len(nnum) == 0 {

				prstr = strings.Replace(prstr, "\n", " ", -1)

				regprstr := reg.ReplaceAllString(prstr, " ")

				if len(strings.TrimSpace(regprstr)) > 1 {

					onlywords = append(onlywords, regprstr)
				}
			} else {

				print("number ", prstr, "\n")
			}

		}

	}

	if len(onlywords) > 0 {

		clphrase := strings.Join(onlywords, " ")

		for i := 0; i < 5; i++ {
			clphrase = strings.Replace(clphrase, "..", ".", -1)
			clphrase = strings.Replace(clphrase, "??", "?", -1)
			clphrase = strings.Replace(clphrase, "!!", "!", -1)
			clphrase = strings.Replace(clphrase, "  ", " ", -1)
			clphrase = strings.Replace(clphrase, " .", ".", -1)
			clphrase = strings.Replace(clphrase, " ?", "?", -1)
			clphrase = strings.Replace(clphrase, " !", "!", -1)

		}

		preproceed = strings.Fields(clphrase)

		var outres []string

		if len(preproceed) > 0 {

			for _, prstr := range preproceed {

				if len(prstr) > 1 {

					outres = append(outres, prstr)

				}

			}

		}

		return true, strings.TrimSpace(strings.Join(outres, " "))
	}

	return false, ""

}
