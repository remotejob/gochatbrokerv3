package configbroadcast

import (
	"bufio"
	"database/sql"
	"encoding/csv"
	"log"
	"os"

	"github.com/spf13/viper"
	"gitlab.com/remotejob/gochatbrokerv3/internal/domains"
)

type Constants struct {
	SLEEP int

	Url          string
	Urlmsg       string
	Urlimg       string
	Imageservice struct {
		Url string
	}
	Mlservice struct {
		Url string
	}
	Wsserver struct {
		Url string
	}
	Sqlitedb struct {
		Dblocation string
	}
	Greetings struct {
		Url string
	}
	Helloagain struct {
		Url string
	}
	Wakeup struct {
		Url string
	}

	Ads struct {
		Url string
	}
}

type Config struct {
	Constants
	Wsclients map[string]domains.Wsclient

	Defaultimg      domains.Imgobj
	Db              *sql.DB
	Greetingslice   []string
	Helloagainslice []string
	Wakeupslice     []string
	Adsslice        []string

	// Hits            int64
	// Clnutsdb        *nutsdb.DB
	// DefaultClient   domains.Client
}

func New() (*Config, error) {
	config := Config{}
	constants, err := initViper()
	if err != nil {
		return &config, err
	}
	config.Constants = constants

	wsclients := make(map[string]domains.Wsclient)
	config.Wsclients = wsclients
	// broadcast := make(chan domains.Message) // broadcast channel
	// broadcast := make(chan domains.Broadcastmsg) // broadcast channel
	// config.Broadcast = broadcast

	litedb, err := sql.Open("sqlite3", config.Sqlitedb.Dblocation)
	if err != nil {
		return &config, err

	}

	config.Db = litedb

	csvfile, err := os.Open(config.Constants.Greetings.Url)
	if err != nil {
		log.Fatalln("Couldn't open the csv file", err)
	}
	r := csv.NewReader(bufio.NewReader(csvfile))
	r.Comma = '\t'
	result, err := r.ReadAll()
	if err != nil {
		log.Fatalln("Couldn't open the csv file", err)
	}
	for _, res := range result {

		config.Greetingslice = append(config.Greetingslice, res[0])

	}

	csvfile2, err := os.Open(config.Constants.Helloagain.Url)
	if err != nil {
		log.Fatalln("Couldn't open the csv file", err)
	}
	r2 := csv.NewReader(bufio.NewReader(csvfile2))
	r2.Comma = '\t'
	result2, err := r2.ReadAll()
	if err != nil {
		log.Fatalln("Couldn't open the csv file", err)
	}

	for _, res2 := range result2 {

		config.Helloagainslice = append(config.Helloagainslice, res2[0])

	}

	csvfile3, err := os.Open(config.Constants.Wakeup.Url)
	if err != nil {
		log.Fatalln("Couldn't open the csv file", err)
	}
	r3 := csv.NewReader(bufio.NewReader(csvfile3))
	r3.Comma = '\t'
	result3, err := r3.ReadAll()
	if err != nil {
		log.Fatalln("Couldn't open the csv file", err)
	}

	for _, res3 := range result3 {

		config.Helloagainslice = append(config.Wakeupslice, res3[0])

	}
	csvfile4, err := os.Open(config.Constants.Ads.Url)
	if err != nil {
		log.Fatalln("Couldn't open the csv file", err)
	}
	r4 := csv.NewReader(bufio.NewReader(csvfile4))
	r4.Comma = '\t'
	result4, err := r4.ReadAll()
	if err != nil {
		log.Fatalln("Couldn't open the csv file", err)
	}

	for _, res4 := range result4 {

		config.Adsslice = append(config.Adsslice, res4[0])

	}

	return &config, err

}

func initViper() (Constants, error) {
	viper.SetConfigName("broadcast.config") // Configuration fileName without the .TOML or .YAML extension
	viper.AddConfigPath(".")                // Search the root directory for the configuration file
	err := viper.ReadInConfig()             // Find and read the config file
	if err != nil {                         // Handle errors reading the config file
		return Constants{}, err
	}

	viper.SetDefault("PORT", "8000")
	if err = viper.ReadInConfig(); err != nil {
		log.Panicf("Error reading config file, %s", err)
	}

	var constants Constants
	err = viper.Unmarshal(&constants)
	return constants, err
}
