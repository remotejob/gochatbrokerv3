package utils

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"

	"gitlab.com/remotejob/gochatbrokerv3/internal/domains"
)

func MakeUniqHistory(modimg []domains.Imgobj) []domains.Imgobj {

	mapimg := make(map[string]domains.Imgobj )
	var res []domains.Imgobj

	for _,img :=range modimg {

		mapimg[img.ID] = img

	}

	for _,v :=range mapimg {

		res = append(res, v)
	}


	return res
}

func GetImg(url string, imgid string) (domains.Imgobj, error) {

	var result domains.Imgobj

	req, err := http.NewRequest("GET", url+imgid, nil)
	if err != nil {

		return result, err
	}
	client := &http.Client{}
	r, err := client.Do(req)
	if err != nil {
		return result, err
	}
	defer r.Body.Close()

	json.NewDecoder(r.Body).Decode(&result)

	return result, nil

}
func Sendmsg(url string, msg domains.Broadcastmsg) {
	bytesRepresentation, err := json.Marshal(msg)
	if err != nil {
		log.Fatalln(err)
	}
	_, err = http.Post(url, "application/json", bytes.NewBuffer(bytesRepresentation))
	if err != nil {
		log.Fatalln(err)
	}
}

func Wshttpget(url string) map[string]domains.Wsclient {

	result := make(map[string]domains.Wsclient)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatalln(err)
	}
	client := &http.Client{}
	r, err := client.Do(req)
	if err != nil {
		log.Fatalln(err)
	}
	defer r.Body.Close()

	json.NewDecoder(r.Body).Decode(&result)

	return result

}

func MlService(url string, ask string) (string, error) {

	var result domains.Payload

	var answer string

	reqbody, err := json.Marshal(map[string]string{"ask": ask})
	if err != nil {
		return answer, err
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(reqbody))
	if err != nil {

		return answer, err

	}
	client := &http.Client{}
	r, err := client.Do(req)
	if err != nil {

		return answer, err
	}
	defer r.Body.Close()

	json.NewDecoder(r.Body).Decode(&result)

	log.Println(result.Answer)
	answer = result.Answer

	return answer, nil

}
