package domains

import (
	"database/sql"
	"time"

	"github.com/gorilla/websocket"
)

type ChatDialog struct {
	Nguid     string  `json:"Nguid"`
	Chatuuid  string  `json:"Chatuuid"`
	Textinorg string  `json:"Textinorg"`
	Textin    string  `json:"Textin"`
	Textout   string  `json:"Textout"`
	Source    string  `json:"Source"`
	Class     string  `json:"Class"`
	Score     float64 `json:"Score"`
	Joinask   int     `json:"Joinask"`
}

type Payload struct {
	Answer string `json:"Answer"`
}
type Askmsg struct {
	Ask string `json:"Ask"`
}

type MsgPayload struct {
	Txt       string `json:"txt,omitempty"`
	Historyok bool   `json:"historyok,omitempty"`
	Img       Imgobj `json:"img,omitempty"`
}

type Broadcastmsg struct {
	ID      string `json:"id"`
	Nguid   string `json:"nguid"`
	Type    string `json:"type"`
	Payload string `json:"payload"`
}

type Wsclient struct {
	Created    time.Time
	Updated    time.Time
	Status     string
	Newcl      bool
	Ws         *websocket.Conn `json:"-"`
	Ask        string
	Lastask    string
	Answer     string
	Lastanswer string
	Count      int64
	Chatuuid   string
	Img        string
	History    bool
	Ad         int
	Wakeup     int
}

type Client struct {
	ID           string    `json:"ID"`
	Created      time.Time `json:"Created"`
	Updated      time.Time `json:"Updated"`
	Cookie       string    `json:"Cookie"`
	Registration string    `json:"Registration"`
	Name         string    `json:"Name"`
	Age          int64     `json:"Age"`
	City         string    `json:"City"`
	Avatar       string    `json:"Avatar"`
	Sex          string    `json:"Sex"`
	Countvisit   int64     `json:"Countvisit"`
	Clicks       int64     `json:"Clicks"`
	Lastimg      Imgobj    `json:"Lastimg"`
	History      []Imgobj  `json:"History,omitempty"`
}

// type Message struct {
// 	Id       string   `json:"id"`
// 	Type     string   `json:"type"`
// 	Nguid    string   `json:"nguid"`
// 	Chatuuid string   `json:"chatuuid"`
// 	Username string   `json:"username"`
// 	Message  string   `json:"message"`
// 	Img      Imgobj   `json:"img"`
// 	History  []Imgobj `json:"history"`
// }

type Imgobj struct {
	ID         string         `json:"id"`
	CreatedAt  time.Time      `json:"createdat"`
	UpdatedAt  time.Time      `json:"-"`
	Name       string         `json:"name"`
	Phone      string         `json:"phone"`
	City       string         `json:"city"`
	Age        int            `json:"age"`
	Sex        string         `json:"sex"`
	Extra      sql.NullString `json:"extra"`
	Selected   int            `json:"selected"`
	Scope      string         `json:"scope"`
	Lastresult string         `json:"-"`
}

// type Image struct {
// 	Timeid        string
// 	Id            int
// 	Name          string
// 	Age           int
// 	City          string
// 	Phone         string
// 	Img_file_name string
// }
// type DefImage struct {
// 	Nguid string `json:"Nguid"`
// 	Img   Image
// }
