package main

import (
	"log"
	"time"

	"gitlab.com/remotejob/gochatbrokerv3/internal/configbroadcast"
	"gitlab.com/remotejob/gochatbrokerv3/internal/domains"
	"gitlab.com/remotejob/gochatbrokerv3/internal/utils"

	"gitlab.com/remotejob/gochatbrokerv3/pkg/elabstatus"

	_ "gitlab.com/remotejob/go-sqlite3"
)

var (
	conf     *configbroadcast.Config
	sleepdur time.Duration
	err      error
	client   domains.Client
	newcl    bool
)

func init() {
	conf, err = configbroadcast.New()
	if err != nil {
		log.Panicln("Configuration error", err)
	}

	sleepdur = time.Duration(conf.SLEEP) * time.Second

}

func main() {

	for {

		wsclients := utils.Wshttpget(conf.Url)
		log.Println("connected", len(wsclients))

		for k, v := range wsclients {

			elabstatus.Elab(conf, k, v, sleepdur)

		}

		time.Sleep(sleepdur)
	}

}
