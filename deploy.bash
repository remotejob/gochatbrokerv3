
scp broadcast.config.toml.prod root@142.93.177.207:gochatvuewsv3/broadcast.config.toml
scp data/hellophrase.csv root@142.93.177.207:gochatvuewsv3/data
scp data/helloagain.csv root@142.93.177.207:gochatvuewsv3/data
scp data/wakeup.csv root@142.93.177.207:gochatvuewsv3/data

go build -o bin/broadcast gitlab.com/remotejob/gochatbrokerv3/cmd/broadcast && \
scp bin/broadcast root@142.93.177.207:/tmp && \
ssh root@142.93.177.207 systemctl stop broadcast.service && \
ssh root@142.93.177.207 mv /tmp/broadcast gochatvuewsv3/ && \
ssh root@142.93.177.207 systemctl start broadcast.service
